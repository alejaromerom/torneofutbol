import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;


public class Principal {


    public static void main(String[] args) {

        Scanner lectura =  new Scanner(System.in);
        Random rand = new Random(System.nanoTime());
        String teamName = "";
        String namePlayer = "";
        String nameCoach = "";
        String positionPlayer = "";
        ArrayList<Team> teams = new ArrayList<>();
        ArrayList<Players> players = new ArrayList<>();
        for (int i=0;i<4; i ++){
            System.out.println("DIGITE EL NOMBRE DEL EQUIPO");
            teamName = lectura.next();
            System.out.println("DIGITE EL NOMBRE DEL ENTRENADOR");
            nameCoach = lectura.next();
            Coach coach = new Coach(nameCoach);
            for (int j = 0;j<5; j++)
            {
                System.out.println("DIGITE EL NOMBRE DEL JUGADOR");
                namePlayer = lectura.next();
                System.out.println("DIGITE EL NUMERO DEL JUGADOR");
                positionPlayer = lectura.next();
                Players player =  new Players(namePlayer,positionPlayer);
                players.add(player);
            }
            Team team1 =  new Team(teamName,players,coach);
            teams.add(team1);
        }

        System.out.println("GENRANDO SORTEO");

        System.out.println("EQUIPO 1 " + teams.get(rand.nextInt(3)).getTeamName() + " vs " +  "EQUIPO 2 " + teams.get(rand.nextInt(3)).getTeamName());

        System.out.println(" EQUIPO 3" + teams.get(rand.nextInt(3)).getTeamName() + " vs " +  " EQUIPO 4 " + teams.get(rand.nextInt(3)).getTeamName());

    }
}
