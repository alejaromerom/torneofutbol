import java.util.ArrayList;

public class Team {


    private String teamName;
    private ArrayList<Players> players;
    private Coach coach;

    public Team(String teamName, ArrayList<Players> players, Coach coach) {
        this.teamName = teamName;
        this.players = players;
    }

    public String getTeamName() {
        return teamName;
    }

    public void setTeamName(String teamName) {
        this.teamName = teamName;
    }

    public ArrayList<Players> getPlayers() {
        return players;
    }

    public void setPlayers(ArrayList<Players> players) {
        this.players = players;
    }
}